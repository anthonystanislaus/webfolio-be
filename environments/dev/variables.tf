variable "project_id" {
  type        = string
  description = "GCP project ID"

  validation {
    condition     = can(regex("^[a-z]([-a-z0-9]*[a-z0-9])?$", var.project_id))
    error_message = "Project name must be lowercase, start with a letter, and can only contain letters, numbers, hyphens, and end with a letter or number."
  }
}

variable "default_region" {}

variable "google_credentials" {}

variable "allow_list" {}

variable "allowed_methods" {}

variable "datastore_kind" {}

variable "datastore_name" {}

variable "datastore_property" {}

variable "infrastructure_services" {
  type        = list(string)
  description = "Platform APIs for provisioning project resources."

  default = [
    # below enables => bigquery/migration/storage, datastore, sql-component, storage/component/api
    "cloudapis.googleapis.com",
    "logging.googleapis.com",
    "monitoring.googleapis.com",
    "cloudtrace.googleapis.com",
    "iam.googleapis.com",
    "compute.googleapis.com",
    "servicecontrol.googleapis.com",
    # remaining may need to be enabled manually
    "serviceusage.googleapis.com",
    "servicemanagement.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "resourcesettings.googleapis.com",
    # below enables => firebaserules
    "firestore.googleapis.com"
  ]
}

variable "required_services" {
  type        = list(string)
  description = "Resource APIs specific to the modules."

  default = [
    # below enables => artifact/container
    "cloudbuild.googleapis.com",
    "run.googleapis.com",
    # below enables => source, pubsub
    "cloudfunctions.googleapis.com",
    "apigateway.googleapis.com"
  ]
}
