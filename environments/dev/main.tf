locals {
  source_directory            = "../../webfolio_be"
  visitors_function_directory = "${local.source_directory}/visitors_function"
}

module "dev_webfolio_be" {
  source                      = "../../modules"
  project_id                  = var.project_id
  region                      = var.default_region
  allow_list                  = var.allow_list
  allowed_methods             = var.allowed_methods
  datastore_kind              = var.datastore_kind
  datastore_name              = var.datastore_name
  datastore_property          = var.datastore_property
  visitors_function_directory = local.visitors_function_directory
  environment                 = "development"
  openapi_contents            = filebase64("${local.visitors_function_directory}/openapi.yaml")

  depends_on = [google_project_service.required_services]
}

import {
  to = module.dev_webfolio_be.google_firestore_database.datastore_default
  id = "(default)"
}
