terraform {
  required_version = ">= 1.5.0"

  cloud {
    organization = "anthony-stanislaus"

    workspaces {
      name = "dev-webfolio-be"
    }
  }
}