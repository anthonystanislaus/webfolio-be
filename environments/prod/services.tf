resource "google_project_service" "infrastructure_services" {
  for_each                   = toset(var.infrastructure_services)
  service                    = each.value
  disable_dependent_services = true
  disable_on_destroy         = false
}

resource "google_project_service" "required_services" {
  for_each                   = toset(var.required_services)
  service                    = each.value
  disable_dependent_services = true
  disable_on_destroy         = true

  depends_on = [google_project_service.infrastructure_services]
}
