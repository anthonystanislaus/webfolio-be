import os
import functions_framework

from google.cloud import datastore
from flask import jsonify, make_response
from functools import wraps


client = datastore.Client()

ALLOW_LIST = os.environ.get("ALLOW_LIST").split(",")
KIND = os.environ.get("KIND")
NAME = os.environ.get("NAME")
PROPERTY = os.environ.get("PROPERTY")
METHOD = os.environ.get("METHOD")


def request_validation(request):
    @wraps(request)
    def _request_validation(args):
        origin = args.headers.get("Origin")
        method = args.method

        if method != METHOD:
            return "", 405
        elif origin not in ALLOW_LIST:
            return "", 403
        else:
            headers = {
                "Access-Control-Allow-Origin": origin,
                "Access-Control-Allow-Methods": method,
                "Access-Control-Allow-Headers": "Content-Type",
                "Access-Control-Max-Age": "400"
            }
            return request(args, headers)

    return _request_validation


@functions_framework.http
@request_validation
def get_visitor_count(request, headers):
    key = client.key(KIND, NAME)
    entity = client.get(key)
    response = make_response()

    if entity is None:
        entity = datastore.Entity(key=key)
        entity[PROPERTY] = 0

    entity[PROPERTY] += 1
    client.put(entity)

    response.data = jsonify({"visitor_count": entity[PROPERTY]}).get_data()
    response.mimetype = "application/json"
    response.headers.update(headers)
    response.status_code = 200

    return response
