resource "google_storage_bucket" "source_files" {
  name                        = "gcf-source-files"
  location                    = var.region
  uniform_bucket_level_access = true
  force_destroy               = true

  labels = {
    environment = var.environment
  }
}

resource "google_storage_bucket_object" "visitors_function_object" {
  bucket         = google_storage_bucket.source_files.name
  name           = "${var.visitors_function_name}-source"
  source         = data.archive_file.visitors_function_zip.output_path
  content_type   = "application/zip"
  detect_md5hash = filemd5("${var.visitors_function_directory}/main.py")

  metadata = {
    environment = var.environment
  }
}

data "archive_file" "visitors_function_zip" {
  type        = "zip"
  source_dir  = var.visitors_function_directory
  output_path = "${path.root}/visitors_function.zip"
}
