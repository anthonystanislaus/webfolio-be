Module: Webfolio BE
---
#### Description:
Used to deploy faas based application

Services:
- Storage
- Functions
- Firestore (Datastore Mode)
- API Gateway
- IAM (including Service Accounts)
