resource "google_api_gateway_api" "public_gateway_api" {
  provider     = google-beta
  api_id       = "public-gateway-api"
  display_name = "Public Gateway API"

  labels = {
    environment = var.environment
  }
}

resource "google_api_gateway_api_config" "public_gateway_spec" {
  provider      = google-beta
  api           = google_api_gateway_api.public_gateway_api.api_id
  display_name  = "Public Gateway API Spec"
  api_config_id = "public-gateway-spec"

  openapi_documents {
    document {
      contents = var.openapi_contents
      path     = var.openapi_path
    }
  }

  gateway_config {
    backend_config {
      google_service_account = google_service_account.apigateway_sa.email
    }
  }

  lifecycle {
    create_before_destroy = true
  }

  labels = {
    environment = var.environment
  }
}

resource "google_api_gateway_gateway" "public_gateway" {
  provider     = google-beta
  api_config   = google_api_gateway_api_config.public_gateway_spec.id
  gateway_id   = "public-gateway"
  display_name = "Public Gateway"

  labels = {
    environment = var.environment
  }
}
