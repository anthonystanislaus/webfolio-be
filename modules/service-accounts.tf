resource "google_service_account" "datastore_sa" {
  account_id   = "datastore-sa"
  display_name = "Datastore Service Account"
  description  = "Used to interact with Firestore database."
}

resource "google_service_account" "apigateway_sa" {
  account_id   = "apigateway-sa"
  display_name = "API Gateway Service Account"
  description  = "Used to authenticate with backend resources."
}

resource "google_project_iam_member" "datastore_user" {
  project = var.project_id
  member  = google_service_account.datastore_sa.member
  role    = "roles/datastore.user"
}
