resource "google_cloudfunctions2_function" "visitors_function" {
  name     = var.visitors_function_name
  location = var.region

  labels = {
    environment = var.environment
  }

  build_config {
    runtime     = "python311"
    entry_point = "get_visitor_count"

    source {
      storage_source {
        bucket = google_storage_bucket.source_files.name
        object = google_storage_bucket_object.visitors_function_object.name
      }
    }
  }

  service_config {
    max_instance_count    = 1
    available_memory      = "256M"
    timeout_seconds       = 60
    ingress_settings      = "ALLOW_ALL"
    service_account_email = google_service_account.datastore_sa.email

    environment_variables = {
      ALLOW_LIST = var.allow_list
      KIND       = var.datastore_kind
      METHOD     = var.allowed_methods
      NAME       = var.datastore_name
      PROPERTY   = var.datastore_property
    }
  }
}

resource "google_cloudfunctions2_function_iam_member" "visitors_function_invoker" {
  cloud_function = google_cloudfunctions2_function.visitors_function.name
  location       = google_cloudfunctions2_function.visitors_function.location
  member         = google_service_account.apigateway_sa.member
  role           = "roles/cloudfunctions.invoker"
}

# seems for unauthenticated access
# Cloud Run allUsers is required in conjunction with cloud function allUsers
resource "google_cloud_run_service_iam_member" "visitors_run_invoker" {
  service  = google_cloudfunctions2_function.visitors_function.name
  location = google_cloudfunctions2_function.visitors_function.location
  member   = google_service_account.apigateway_sa.member
  role     = "roles/run.invoker"
}
