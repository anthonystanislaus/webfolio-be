resource "google_firestore_database" "datastore_default" {
  name        = "(default)"
  location_id = var.region
  type        = "DATASTORE_MODE"

  timeouts {}
}
