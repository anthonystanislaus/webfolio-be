variable "environment" {
  type        = string
  description = "Describes the execution environment."
}

variable "project_id" {
  type        = string
  description = "GCP project's ID."
}

variable "region" {
  type        = string
  description = "GCP project's default region."
}

variable "allow_list" {
  type = string
}

variable "allowed_methods" {
  type = string
}

variable "datastore_kind" {
  type = string
}

variable "datastore_name" {
  type = string
}

variable "datastore_property" {
  type = string
}

variable "visitors_function_directory" {
  type        = string
  description = "Path to visitor function's source code."
}

variable "visitors_function_name" {
  type    = string
  default = "visitor-count"
}

variable "openapi_path" {
  type        = string
  description = "?"
  default     = "spec.yaml"
}

variable "openapi_contents" {
  type        = string
  description = "Base64 encoded openapi content."
}
