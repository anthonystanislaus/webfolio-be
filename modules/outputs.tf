output "public_gateway_url" {
  value = google_api_gateway_gateway.public_gateway.default_hostname
}
